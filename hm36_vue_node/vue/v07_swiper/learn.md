是一个滑动库, 非常丰富的滑动样式
```shell
https://www.swiper.com.cn
https://github.com/surmon-china/vue-awesome-swiper
```
# 1, 基本
### 1.1) 安装
```shell
npm install vue-awesome-swiper --save
```

### 1.2) 全局引用
```shell
import Vue from 'vue'
import VueAwesomeSwiper from 'vue-awesome-swiper'

// require styles
import 'swiper/dist/css/swiper.css'

Vue.use(VueAwesomeSwiper, /* { default global options } */)
```
### 1.3) 局部引入
```shell
// require styles
import 'swiper/dist/css/swiper.css'

import { swiper, swiperSlide } from 'vue-awesome-swiper'

export default {
  components: {
    swiper,
    swiperSlide
  }
}
```


