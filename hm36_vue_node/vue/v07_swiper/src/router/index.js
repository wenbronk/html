import Vue from 'vue'
import Router from 'vue-router'

import SSR_Swiper from '@/components/SSR-swiper'
import SPA_Swiper from '@/components/SPA-swiper'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/ssr',
      name: 'SSR_Swiper',
      component: SSR_Swiper
    },
    {
      path: '/spa',
      name: 'SPA_Swiper',
      component: SPA_Swiper,
    }
  ]
})
