import Vue from 'vue'
import Router from 'vue-router'
import index from '@/components/index'
import Course from '@/components/course'
import Master from '@/components/master'
import Java from '@/components/course/java'
import Python from '@/components/course/python'

Vue.use(Router)

export default new Router({
  mode: "history",
  linkActiveClass: "active",
  routes: [
    {
      path: '/',
      name: 'index',
      component: index
    },
    {
      path: '/course',
      name: 'Course',
      component: Course,
      // 默认进入重定向
      redirect: "/course/java",
      // 子嵌套
      children: [
        {
          path: "java",
          name: "Java",
          component: Java
        },
        {
          path: "python",
          name: "Python",
          component: Python
        }
      ]
    },
    {
      path: '/master/:count/:type',
      name: "Master",
      component: Master
    }
  ]
})
