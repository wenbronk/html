import Vue from 'vue'

// 引入 router
import VueRouter from "vue-router"

import HelloWorld from '../components/HelloWorld'
import HellowIwen from '../components/HellowIwen'

Vue.use(VueRouter)

export default new VueRouter({
  routes: [{
    path: "/hello",
    component: HelloWorld
  }, {
    path: "/iwen",
    component: HellowIwen
  }]
})
