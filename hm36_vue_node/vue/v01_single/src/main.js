// 引入vue
import Vue from 'vue'

// 引入app.vue， 用App的内容替换 div
import App from './App'

// 开发环境配置
Vue.config.productionTip = false

/* 创建vue实例 */
new Vue({
  // 渲染内容的目的地， 绑定元素
  el: '#app',
  // 渲染内容
  // render: function (creater) {
  //   return creater(App)
  // }
  template: '<App/>',
  components: { App }
})
