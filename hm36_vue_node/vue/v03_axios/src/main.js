// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
// 引入
import Axios from 'axios'

Vue.config.productionTip = false

// 挂在在Vue上
Vue.prototype.$axios=Axios;

// 解决跨域:
Vue.prototype.HOST = '/api'

// 在之后不需要 写 网址， uri了
// Axios.defaults.baseURL = 'https://api.example.com';
// // https 认证
// Axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
// // 设置heads， 可以不需要qs设置
// Axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

import qs from 'qs'
// 拦截器
Axios.interceptors.request.use(function (config) {
  // 在发送请求之前做些什么

  if (config.method === 'post') {
    config.data = qs.stringify(config.data)
  }


  return config;
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error);
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>'
})
