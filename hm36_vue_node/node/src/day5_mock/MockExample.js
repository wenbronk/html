var express = require('express')
var app = express()

var Mock = require("mockjs")

app.engine('html', require('express-art-template'))

app.use('/node_modules/', express.static('./node_modules/'))

// 添加 post， 中间件要在挂在路由之前
var bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

var router = require('./MockRoute.js')
app.use(router)

app.listen(8080, function() {
    console.log("starting...")
})