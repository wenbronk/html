/**
 * 使用 express 框架
 * npm install --save express
 * @type {createApplication}
 */
var express = require('express')

var app = express()

// get 请求  / 路径
// 不需要关心中文 问题
app.get('/', function(request, response) {

    // 获取请求参数
    var queryParams = request.query;

    response.send('hello express, meeting !')
}).post('/ ', function (request, response) {
    response.end('<h1>请求的页面未找到</h1>')
})

// 公开目录, 可通过 public/xxx的方式访问 public下的所有文件
app.use('/public', express.static('./b04_express/public'))


app.listen(8080, function() {
    console.log('server starting')
})



