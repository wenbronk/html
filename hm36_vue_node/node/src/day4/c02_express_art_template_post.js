

var express = require('express')

var app = express()

// 配置使用 art-template 引擎, 如果是art结尾的， 就用 art-template 引擎
// app.engine('art', require('express-art-template'))
app.engine('html', require('express-art-template'))

// 提供render（'html模版名'， {模板数据})方法，
// 默认第一个参数为 view下的文件名， 文件必须 art 格式
// 第二个为数据， 在html中使用  {{ title }} 进行取值
// 如果想要修改， 使用 set
app.set('views', './c02_public/views')
app.get('/', function (request, response) {

    response.render('index.html', {
        title: "留言板系统",
        comments: comments
    })
})

app.get('/post', function (request, response) {
    response.render('post.html')
})

app.use(express.static('./c02_public/public'))


app.listen(8080, function () {
    console.log("running ....")
})

// 获取post请求, 配置body-parse 中间件
var bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.post('/post', function (request, response) {

    // post 获取
    var comment = request.body;
    comment.dateTime = '2018-09-11 10:58:31'
    comments.unshift(comment)

    // get 获取的方式
    // var comment = request.query  // 只能拿get请求参数
    // comment.dateTime = '2018-09-11 10:58:31'
    // comment.unshift(comment)
    response.redirect('/')
})


var comments = [{
    name: '张三',
    message: '今天天气不错！',
    dateTime: '2015-10-16'
},
    {
        name: '张三2',
        message: '今天天气不错！',
        dateTime: '2015-10-16'
    },
    {
        name: '张三3',
        message: '今天天气不错！',
        dateTime: '2015-10-16'
    },
    {
        name: '张三4',
        message: '今天天气不错！',
        dateTime: '2015-10-16'
    },
    {
        name: '张三5',
        message: '今天天气不错！',
        dateTime: '2015-10-16'
    }
]