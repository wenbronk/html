/**
 * 操作文件中的数据
 *
 */

fs = require('fs')
var dbPath = './db.json'

/**
 * 获取学生列表
 * 使用回掉函数
 * return []
 */
exports.find = function (callback) {
    fs.readFile(dbPath, 'utf8', function (err, data) {
        if (err)  return callback(err)
        callback(null, JSON.parse(data).students)
    })
}

exports.findById = function(id, callback) {
    fs.readFile(dbPath, 'utf8', function (err, data) {
        if (err) return callback(err)
        var students = JSON.parse(data).students
        var stu = students.find(function (item) {
            return item.id === id
        })
        callback(null, stu)
    })
}

/**
 * 保存
 */
exports.save = function (student, callback) {
    fs.readFile(dbPath, 'utf8', function (err, data) {
        if (err) return callback(err)
        var students = JSON.parse(data).students

        // 加id
        student.id = students[students.length - 1].id + 1
        students.push(student)
        var fileData = JSON.stringify({
            students: students
        })

        // 写回去
        fs.writeFile(dbPath, fileData, function (err) {
            if (err) return callback(err)
            callback(null)
        })
    })

}

/**
 * 更新
 */
exports.update = function (student, callback) {
    fs.readFile(dbPath, 'utf8', function (err, data) {
        if (err) callback(err)
        var students = JSON.parse(data).students

        // 根据id进行更新
        var updateStu = students.find(function (item) {
            return item.id === parseInt(student.id)
        })

        students.push(student)

        // 写回去
        fs.writeFile(dbPath, JSON.stringify({ students: students}), function (err) {
            if (err) return callback(err)
            callback(null)
        })
    })

}

/**
 * 删除学生
 */
exports.del = function (id, callback) {
    fs.readFile(dbPath, 'utf8', function (err, data) {
        if (err) callback(err)
        var students = JSON.parse(data).students

        // 用来根据条件寻找下标
        var index = students.findIndex(function(item) {
            return item.id === parseInt(id)
        })

        students.splice(index, 1)

        // 写回去
        fs.writeFile(dbPath, JSON.stringify({ students: students}), function (err) {
            if (err) return callback(err)
            callback(null)
        })
    })

}


