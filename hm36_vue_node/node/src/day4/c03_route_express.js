/**
 * 使用 express 提供的路由方式
 */
fs = require('fs')

var Students = require('./c03_file_student')

var express = require('express')

// 创建路由容器
var router = express.Router()

// 挂在路由到router上
router.get('/students', function (request, response) {
    Students.find(function (err, data) {
        if (err) return respones.status(500).send(err)
        response.render('index.html', {
            students: data
        })
    })
})


router.get('/students/new', function (request, response) {
    response.render('new.html')
})

router.post('/students/new', function (request, response) {
    // console.log(request.body)
    // 接受数据， 保存在 db_03.json文件中
    var newStudent = request.body
    newStudent.id = parseInt(newStudent.id)
    // 使用回掉函数读取结果
    Students.save(newStudent, function (err) {
        if (err) response.status(500).send(err)
    })
    response.redirect('/students')
})


router.get('/students/edit', function (request, response) {
    var id = parseInt(request.query.id)
    // 获取学生
    Students.findById(id, function (err, stu) {
        response.render('edit.html', {
            student: stu
        })
    })
})

router.post('/students/edit', function (request, response) {
    var upstu = request.body
    upstu.id = parseInt(upstu.id)
    Students.update(upstu, function (err) {
        if (err) response.status(500).send(err)
        response.redirect('/students')
    })
})

router.get('/students/delete', function (request, response) {
    var id = parseInt(request.query.id)
    // 获取学生
    Students.del(id, function (err) {
        if (err) response.status(500).send(err)
        response.redirect('/students')
    })
})


// 导出router
module.exports = router
