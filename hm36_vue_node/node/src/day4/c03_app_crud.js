var express = require('express')
var app = express()

var fs = require('fs')

app.engine('html', require('express-art-template'))

app.set('views', './c03_public/views')
app.use('/public', express.static('./c03_public/public'))
app.use('/node_modules/', express.static('./node_modules/'))

// app.get('/', function (request, response) {
//     fs.readFile('./db.json', 'utf8', function (err, data) {
//         if (err) return response.status(500).send("server error")
//         console.log(data)
//
//         response.render('index.html', JSON.parse(data))
//     })
// })

// 添加 post， 中间件要在挂在路由之前
var bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())


// route 使用
// 使用function的方式调用
// var router = require('./c03_route')
// router(app)

// 使用 express的方式 使用 router
var router = require('./c03_route_express.js')
app.use(router)

app.listen(8080, function() {
    console.log("starting...")
})