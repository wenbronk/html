/**
 * 使用mapping的简写形式
 *
 */

var express = require('express')

var app = express()

// 以public开头的url， 去 public下寻找， 默认寻找  index。html
// app.use('/public/', express.static('../resource/b04_express/public'))

// 省略第一个参数的时候， uri 也需要省略 public
app.use(express.static('./c02_public/public'))

app.listen(8080, function () {
    console.log('express app is running ....')
})
