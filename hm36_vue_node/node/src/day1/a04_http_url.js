

var http = require('http')

var server = http.createServer()

/**
 * 使用 response.end('') 直接进行发送
 * 响应内容只能是字符串或者二进制数据
 */
server.on('request', function (request, response) {

    var url = request.url
    console.log("receive rquest, url: " + url)

    var products = [
        {
            name: "iphone",
            price: 1234
        },
        {
            name: "1+",
            price: 2345
        }
    ]

    if (url === "/login") {
        response.end('login page response')
    } else if(url === '/regist') {
        response.end('regist page response')
    } else if (url === '/products') {

        // 将数组转字符串
        response.end(JSON.stringify(products))
    } else {

        response.end('404 not found, unknow: ' + url)
    }


})

server.listen(8080, function () {
    console.log("server starting....\n server start")
})

