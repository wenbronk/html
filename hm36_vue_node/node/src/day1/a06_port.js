/**
 * 端口号， 是使用网卡的地址
 * 需要通讯的程序， 必须有一个端口号， 使用 ip：port唯一确定程序
 * 0-65535 过程
 * 系统中有许多默认的端口号
 */

var http = require('http')

var server = http.createServer();

server.on('request', function (request, response) {

    var port = request.socket.remotePort

    console.log("receive from port: " + port)

    response.end("listen from port: " + port)
})

server.listen(8080, function () {
    console.log("server starting....")
})

