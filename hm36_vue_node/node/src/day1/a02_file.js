/**
 * 读取文件， 使用 file-system模块
 * 使用require 进行加载， 但不会对加载文件产生污染
 */

// 导入 file-system
var fs = require('fs')


// 读文件
// 成功 data-》数据， error-》 null
// 失败 data-》null， error -》 错误对象
fs.readFile('./n01_helloworld.js', function (error, data) {

    if (error != null) {
        console.log(error)
        return
    } else {
        console.log(data.toString())
    }
})


// 写文件
// 成功： 文件写入， error是null
// 错误， error有值
fs.writeFile('../../target/writeFile', 'hello world what to write', function (error) {

    if (error == null) {
        console.log(error)
    } else {
        console.log("写入成功")
    }

})
