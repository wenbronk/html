/**
 * 返回中文内容的时候， 需要更改编码为 UTF-8 的编码形式
 */

var http = require('http')
var fs = require('fs')

var server = http.createServer()

server.on('request', function (request, response) {

    console.log("receiver request: " + request.url + " port: " + request.socket.remotePort)

    var url = request.url

    // 设置 header, 普通文本类型
    if (url === '/plain') {
        response.setHeader("Content-Type", "text/plain; charset=utf-8")
        response.end('返回内容为中文的时候， 需要设置content_type ')

    }else if (url === '/html') {
        response.setHeader("Content-Type", "text/html; charset=utf-8")
        response.end("<h1> 返回内容为 html时， 需要设置  text/html  </h1>")

    } else if (url === '/') {
        // 读取html文件
        var data = fs.readFile('/Users/bronkwen/work/WebstormProjects/hm36/day01/src/resource/index.html', function (err, data) {
            if (err) {
                response.setHeader('Content-Type', 'text/plain; charset=utf-8')
                response.end(' 文件读取失败，')
            } else {
                response.setHeader('Content-Type', 'text/html; charset=utf-8')
                response.end(data)
            }
        })
    } else if (url === '/img') {
        //读取 img
        var data = fs.readFile('/Users/bronkwen/work/WebstormProjects/hm36/day01/src/resource/IMG_0001.JPG', function (err, data) {
            if (err) {
                response.setHeader('Content-Type', 'text/plain; charset=utf-8')
                response.end(' 文件读取失败，')
            } else {
                response.setHeader('Content-Type', 'image/jpeg')
                response.end(data)
            }
        })
    }

})

server.listen(8080, function () {
    console.log("server starting")

})
