
// 提供web 服务, 发送响应

// 导入 http模块
var http = require('http')

// 创建web服务器
var server = http.createServer()

// 提供服务
// 注册 request 请求， 自动触发 回掉函数
server.on('request', function (request, response) {
    console.log("receive request " + request.url)

    // 响应, 可以多次响应， 但必须用 end 进行结束
    response.write("call back response\n")
    response.write("second reponse")
    response.end()
})

// 启动服务器
server.listen(8080)

