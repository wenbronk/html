/**
 * 模块化编程，
 *
 * node 中只有 模块作用域， 没有全局作用域
 *
 */

var a = require('../resource/a05_a')
console.log(a.foo)

console.log(' a running end')
